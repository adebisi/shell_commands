# find all the files with a specific extension in the current directory and move them to another directory.
```
find -name '*.mp4' -exec mv {} ~/Videofs/
```
#rsync a dir to another server.
```
rsynch -av "name of the dir" "username"@"ip or hostname":/"dir where its going to"
```
#use borg to make a backup of dirs.
```
borg init repo
```
```
borg create --stats --progress --compression lz4 repo::backup "the dir you want to backup"
```

#use borg to backup using ssh to another server
```
#on the server where the backup is going to 
borg init repo
```
#back at the server where you are backing up 
```
borg create --stats --progress --compression lzma "username"@"hostname or ip":"dir where the repo is/repo"::"name for this backup" "dir to be backup"
```
# see what port a service is using.
```
netstat -tulpn
```

# change swappiness. Change value to your desire.
```
sysctl vm.swappiness=1
```
```
sudo vim /etc/sysctl.conf #and add
vm.swappiness = 1
```

#Change open files for applications. Change value to your desire.
```
sudo vim /etc/security/limits.conf #and add
* hard nofile 99999
* soft nofile 99999
* hard nproc  20000
* soft nproc  20000
```

# Partition drive with gpt
```
sudo parted /dev/"name of disk" #example /dev/vdb or /dev/sdb
```
```
#in the parted menu do ```print ``` to see the disks then do:
mklabel > gpt
mkpart
#name of the disk
#what filesystem to use
# the number of the drive
# the size of the drive in GB
# after this then you format it
```
``
sudo mkfx.xfs /dev/"the drive plus the number" #example /dev/vdb1
```
#then mount it
```
```
sudo vim /etc/fstab
# /dev/vdb1 /data xfs defaults,noatime 00
```
#copy a dir to another dir with a dry run to see what changes. After you are ready remove the --dry-run
```
rsync --verbose --archive --dry-run /oldisk/a/ /newdisk/a/
```

# block IPs
Ah but it is! Use this script:

# Create the ipset list
ipset -N china hash:net
# remove any old list that might exist from previous runs of this script
rm cn.zone
# Pull the latest IP set for China
wget -P . http://www.ipdeny.com/ipblocks/data/countries/cn.zone
# Add each IP address from the downloaded list into the ipset 'china'
for i in $(cat /etc/cn.zone ); do ipset -A china $i; done
# Restore iptables
/sbin/iptables-restore < /etc/iptables.firewall.rules

Then alter your firewall rules:

iptables -A INPUT -p tcp -m set --match-set china src -j DROP


#search for a string in vim and delete it
:%s/phrase to delete//gc


# to boot into emergency mode with systemd.during boot you choose the kernel and hit e to edit
# at the end of the line add
```
systemd.unit=emergency.target
```


```

tr "[:lower:]" "[:upper:]" < input.txt > output.txt

cat file.txt | tr "[:lower:]" "[:upper:]" > output.txt
```


```
"user" ALL=(ALL) NOPASSWD:EXEC:ETENV:ALL
```
```
grub2-mkconfig -o /boot/efi/EFI/fedora/grub.cfg
```
